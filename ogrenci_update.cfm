<cfset dsn='dsn'>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<cfinclude template = "ogrenci_index.cfm">
<cfinclude template = "background.cfm">
<!------ Include the above in your HEAD tag ---------->
<div class="container">
<cfquery name="get_iller" datasource="#dsn#">
SELECT * FROM ILLER</cfquery>
<cfquery name="get_kan" datasource="#dsn#">
SELECT * FROM KAN_GRUPLARI</cfquery>
<cfquery name="get_tel_kod" datasource="#dsn#">
SELECT * FROM TEL_KOD </cfquery>
<cfquery name="get_ogrenci"  datasource="#dsn#">
SELECT * FROM OGRENCI WHERE OGRENCI_ID=#URL.ID#</cfquery>

        <table class="table table-striped">
        
          <tbody>
             <tr>
                <td colspan="1">
                <cfoutput>
                   <form name="kayit" enctype="multipart/form-data"  name="form" id="form" action="ogrenci_update_query.cfm?id=#url.id#" method="post" class="well form-horizontal">
                      </cfoutput>
                      <input type="hidden" name="id" value="<cfoutput>#URL.ID#</cfoutput>">
                      <fieldset>
                      <cfoutput query="get_ogrenci">
                         <div class="form-group">
                            <label class="col-md-4 control-label">Ad</label>
                            <div class="col-md-8 inputGroupContainer">
                               <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span><input id="ad" name="ad" placeholder="Name" class="form-control" required="true" value="#OGRENCI_ADI#" type="text"></div>
                            </div>
                         </div>

                         <div class="form-group">
                            <label class="col-md-4 control-label">Soyad</label>
                            <div class="col-md-8 inputGroupContainer">
                               <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span><input id="soyad" name="soyad" placeholder="Surname" class="form-control" required="true" value="#OGRENCI_SOYADI#" type="text"></div>
                            </div>
                         </div>

                         <div class="form-group">
                            <label class="col-md-4 control-label">Tel Kod</label>
                            <div class="col-md-8 inputGroupContainer">
                               <div class="input-group">
                                  <span class="input-group-addon" style="max-width: 100%;"><i class="glyphicon glyphicon-phone"></i></span>
                                  <select name="tel_kod" id="tel_kod" class="selectpicker form-control">
                                     <option>Seçiniz</option>
                                     <cfloop query="get_tel_kod">
                                     <option value="get_tel_kod.#TEL_KOD_ID#" <cfif get_ogrenci.TEL_KOD eq get_tel_kod.TEL_KOD_ID>selected</cfif>>#TEL_KOD_DEGER#</option>
                                     </cfloop>
                                  </select>
                               </div>
                            </div>
                         </div>

                         <div class="form-group">
                            <label class="col-md-4 control-label">Telefon</label>
                            <div class="col-md-8 inputGroupContainer">
                               <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-earphone"></i></span><input id="tel" name="tel" placeholder="Phone Number" class="form-control" required="true" value="#TEL#" type="text"></div>
                            </div>
                         </div>
                         
                         <div class="form-group">
                            <label class="col-md-4 control-label">Doğum Tarihi</label>
                            <div class="col-md-8 inputGroupContainer">
                               <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span><input id="dtar" name="dtar" placeholder="Date Of Birth" class="form-control" required="true" value="#DOGUM_TARIHI#" type="date"></div>
                            </div>
                         </div>

                         <div class="form-group">
                            <label class="col-md-4 control-label">Doğum Yeri</label>
                            <div class="col-md-8 inputGroupContainer">
                               <div class="input-group">
                                  <span class="input-group-addon" style="max-width: 100%;"><i class="glyphicon glyphicon-list"></i></span>
                                  <select name="dyer" id="dyer" class="selectpicker form-control">
                                     <option>Seçiniz</option>
                                     <cfloop query="get_iller">
                                     <option value="#get_iller.DOGUM_YERI_ID#" <cfif get_ogrenci.DOGUM_YERI_ID eq get_iller.DOGUM_YERI_ID>selected</cfif>>#DOGUM_YERI#</option>
                                     </cfloop>
                                  </select>
                               </div>
                            </div>
                         </div>

                         <div class="form-group">
                            <label class="col-md-4 control-label">Email</label>
                            <div class="col-md-8 inputGroupContainer">
                               <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span><input id="email" name="email" placeholder="Email" class="form-control" required="true" value="#MAIL_ADRESI#" type="text"></div>
                            </div>
                         </div>

                         <div class="form-group">
                            <label class="col-md-4 control-label">Ev Adresi</label>
                            <div class="col-md-8 inputGroupContainer">
                               <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span><input id="adres" name="adres" placeholder="Home Adress" class="form-control" required="true" value="#EV_ADRESI#" type="text"></div>
                            </div>
                         </div>
        
                         <div class="form-group">
                            <label class="col-md-4 control-label">Kan Grubu</label>
                            <div class="col-md-8 inputGroupContainer">
                               <div class="input-group">
                                  <span class="input-group-addon" style="max-width: 100%;"><i class="glyphicon glyphicon-tint"></i></span>
                                  <select name="kan" id="kan" class="selectpicker form-control">
                                     <option>Seçiniz</option>
                                     <cfloop query="get_kan">
                                     <option value="#get_kan.KAN_GURUB_ID#" <cfif get_ogrenci.KAN_GURUBU_ID eq get_kan.KAN_GURUB_ID>selected</cfif>>#KAN_GURUBU#</option>
                                     </cfloop>
                                  </select>
                               </div>
                            </div>
                         </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label">Fotoğraf</label>
                         <div class="col-md-8 inputGroupContainer">
                         <div class="input-group">
                         <img class="img-circle" alt="Cinque Terre" style="width:150px; height:150px;" src="images/#FOTOGRAF#.jpg" name="myImage"></div>
                        </div>


                       <div class="form-group">
                            <label class="col-md-4 control-label"></label>
                            <div class="col-md-8 inputGroupContainer">
                               <div class="input-group"><span class="input-group-addon"><label for="exampleFormControlFile1"><i class="glyphicon glyphicon-camera"></i></label>
                               <input name="angrs" id="kkln" value="" type="hidden">
                               <input type="file" id="customFile" name="foto" id="foto" class="form-control-file" id="exampleFormControlFile1" >
                               </div></span></div>
                            </div>
                       
                      
                   </fieldset>
                </td>
                <td colspan="1">
                   <div class="well form-horizontal">
                      <fieldset>
                         <div class="form-group">
                            <label class="col-md-4 control-label">Acil Durum Kişisi</label>
                            <div class="col-md-8 inputGroupContainer">
                               <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span><input id="ak_kisi" name="ak_kisi" placeholder="Full Name" class="form-control" required="true" value="#ACIL_DURUM_KISI#" type="text"></div>
                            </div>
                         </div>

                         <div class="form-group">
                            <label class="col-md-4 control-label">Tel Kod</label>
                            <div class="col-md-8 inputGroupContainer">
                               <div class="input-group">
                                  <span class="input-group-addon" style="max-width: 100%;"><i class="glyphicon glyphicon-phone"></i></span>
                                  <select name="ak_tel_kod" id="ak_tel_kod" class="selectpicker form-control">
                                     <option>Seçiniz</option>
                                     <cfloop query="get_tel_kod">
                                     <option value="get_tel_kod.#TEL_KOD_ID#" <cfif get_ogrenci.TEL_KOD eq get_tel_kod.TEL_KOD_ID>selected</cfif>>#TEL_KOD_DEGER#</option>
                                     </cfloop>
                                  </select>
                               </div>
                            </div>
                         </div>

                         <div class="form-group">
                            <label class="col-md-4 control-label">Telefon</label>
                            <div class="col-md-8 inputGroupContainer">
                               <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-earphone"></i></span><input id="ak_tel" name="ak_tel" placeholder="Phone Number" class="form-control" required="true" value="#ACIL_DURUM_KISI_TEL#" type="text"></div>
                            </div>
                         </div>

                         <div class="form-group">
                            <label class="col-md-4 control-label">Email</label>
                            <div class="col-md-8 inputGroupContainer">
                               <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span><input id="ak_email" name="ak_email" placeholder="Email" class="form-control" required="true" value="#ACIL_DURUM_KISI_MAIL#" type="text"></div>
                            </div>
                         </div>
                         </cfoutput>
                      </fieldset>
                   <div align="right"><button onclick="return Valid()" type="submit" class="btn btn-info">Güncelle</button>
                  <a href="ogrenci_query_delete.cfm?id=#ogrenci_id#"><img src="deleteikon.png" width="30px" height="30px"/></a>
                </td>
             </tr> 
             
             </form>
          </tbody>
       </table>
    </div>


    <script>
    function Valid() {
        debugger;
       var gorsel = $('#customFile');
       if (gorsel[0].files.length == 0) {

           document.getElementById("kkln").value = "0";

       }else{
        document.getElementById("kkln").value = "1";

       }
       }
</script>
 


     