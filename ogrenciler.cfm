﻿<cfset dsn='dsn'>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<cfinclude template = "ogrenci_index.cfm">
<cfinclude template = "background.cfm">
<!------ Include the above in your HEAD tag ---------->
<cfquery name="get_kayitlar" datasource="#dsn#">
SELECT * FROM OGRENCI</cfquery>
<form>
<div align="right">
<img src="search.png" width="20px" height="20px"> 
<input type="text" autocomplete="off" name="myInput" id="myInput" placeholder="Ara"/>
</div>
<table class="table">
  <thead>
    <tr>
      <th scope="col">Sıra</th>
      <th scope="col">Adı</th>
      <th scope="col">Soyadı</th>
      <th scope="col">Düzenle</th>
    </tr>
  </thead>
  <tbody  id="myTable">
  <cfoutput query="get_kayitlar">
    <tr>
      
      <th scope="row">#currentrow#</th>
      <td>#OGRENCI_ADI#</td>
      <td>#OGRENCI_SOYADI#</td>
      <th><a href="ogrenci_update.cfm?id=#ogrenci_id#"><img src="editikon.png" width="20px" height="20px"></a>
	        <a href="ogrenci_query_delete.cfm?id=#ogrenci_id#" onclick="return confirm('Silmek İstediğinize emin misiniz?')"><img src="deleteikon.png" width="20px" height="20px"/></a></th>
      <th><a href="ogrenci_detay.cfm?id=#ogrenci_id#"><img src="eye_icon.png" width="30px" height="30px"></a></th>
    </tr>
    </cfoutput>
  </tbody>
</table>
</form>

<script>
 $(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});

 </script>
